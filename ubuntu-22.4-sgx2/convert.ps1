function GetStringBetweenTwoStrings($firstString, $secondString, $importPath){

    #Get content from file
    $file = Get-Content $importPath

    #Regex pattern to compare two strings
    $pattern = "$firstString(.*?)$secondString"

    #Perform the opperation
    $result = [regex]::Match($file,$pattern).Groups[1].Value

    #Return result
    return $result

}

# get ip from hyperv-vm:
Get-VM Ubuntu22-4-sgx1 | Select-Object -ExpandProperty NetworkAdapters |Select-Object IPAddresses| Out-File .\IP1
#parsing IP
GetStringBetweenTwoStrings -firstString "{" -secondString "," -importPath ".\IP1"