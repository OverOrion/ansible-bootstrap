#!/bin/bash

sudo usermod -a -G sgx_prv ubuntu
sudo cp ./configs/10-sgx.rules /etc/udev/rules.d
sudo udevadm trigger


#nodejs

sudo apt install sqlite3 build-essential -y
sudo apt install libsgx-quote-ex-dev -y
sudo apt install cracklib-runtime -y

curl -o setup.sh -sL https://deb.nodesource.com/setup_14.x
chmod a+x setup.sh
./setup.sh
sudo apt-get -y install nodejs

#intel:repo

echo 'deb [arch=amd64] https://download.01.org/intel-sgx/sgx_repo/ubuntu focal main' > /etc/apt/sources.list.d/intel-sgx.list
wget -O - https://download.01.org/intel-sgx/sgx_repo/ubuntu/intel-sgx-deb.key | apt-key add -
apt update

######apt install sgx-dcap-pccs ansible vel

#remote attetsthez

wget http://mirrors.kernel.org/ubuntu/pool/main/p/protobuf/libprotobuf17_3.6.1.3-2ubuntu5_amd64.deb
dpkg -i libprotobuf17_3.6.1.3-2ubuntu5_amd64.deb

apt-get install -y \
        libsgx-headers \
        libsgx-ae-epid \
        libsgx-ae-le \
        libsgx-ae-pce \
        libsgx-aesm-ecdsa-plugin \
        libsgx-aesm-epid-plugin \
        libsgx-aesm-launch-plugin \
        libsgx-aesm-pce-plugin \
        libsgx-aesm-quote-ex-plugin \
        libsgx-enclave-common \
        libsgx-enclave-common-dev \
        libsgx-epid \
        libsgx-epid-dev \
        libsgx-launch \
        libsgx-launch-dev \
        libsgx-quote-ex \
        libsgx-quote-ex-dev \
        libsgx-uae-service \
        libsgx-urts \
        sgx-aesm-service \
        libsgx-ae-qe3 \
        libsgx-pce-logic \
        libsgx-qe3-logic \
        libsgx-ra-network \
        libsgx-ra-uefi \
        libsgx-dcap-default-qpl-dev && \
    apt-get clean -y

# meg ket library

wget https://download.01.org/intel-sgx/latest/linux-latest/distro/ubuntu20.04-server/debian_pkgs/libs/libsgx-dcap-quote-verify/libsgx-dcap-quote-verify_1.13.100.4-focal1_amd64.deb
wget https://download.01.org/intel-sgx/latest/linux-latest/distro/ubuntu20.04-server/debian_pkgs/devel/libsgx-dcap-quote-verify-dev/libsgx-dcap-quote-verify-dev_1.13.100.4-focal1_amd64.deb
dpkg -i ./libsgx-dcap-quote-verify_1.13.100.4-focal1_amd64.deb
dpkg -i ./libsgx-dcap-quote-verify-dev_1.13.100.4-focal1_amd64.deb

get http://mirrors.kernel.org/ubuntu/pool/main/p/protobuf/libprotobuf17_3.6.1.3-2ubuntu5_amd64.deb
dpkg -i libprotobuf17_3.6.1.3-2ubuntu5_amd64.deb

echo "deb http://security.ubuntu.com/ubuntu focal-security main" | sudo tee /etc/apt/sources.list.d/focal-security.list
sudo apt-get update

sudo apt --fix-broken install -y
sudo apt-get install libssl1.1 -y
sudo apt-get install sgx-aesm-service -y

service sgx-dcap-pccs restart

#kube plugin

kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.9.1/cert-manager.yaml

git clone https://github.com/intel/intel-device-plugins-for-kubernetes.git --branch release-0.23
kubectl apply -k ./intel-device-plugins-for-kubernetes/deployments/sgx_plugin/overlays/epc-nfd

sudo apt install redis-tools -y

marblerun install

#jöhet a redis eredeti
