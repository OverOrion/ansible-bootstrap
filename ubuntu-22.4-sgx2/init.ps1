## ansible provisioning script
#wget https://gitlab.com/securitee/infrastructure/ansible-bootstrap/-/archive/main/ansible-bootstrap-main.tar.gz?path=ubuntu-22.4-sgx2 -OutFile bootstrap.tgz
## build base vm
date >log
.\packer.exe build -force .\ubi.json
$dir=$(echo '.\output-hyper-v1\Virtual Machines\')

#base vm
Import-VM -Path $dir$(Get-ChildItem '.\output-hyper-v1\Virtual Machines\' -Filter *.vmcx -name)
#Export-Vm  -VMName Ubuntu22-4-sgx1 .\
#Rename-VM -Name Ubuntu22-4-sgx1 -Newname Ubuntu22-4-sgx1-1

##cloning
#Import-VM -Path $dir$(Get-ChildItem '.\Ubuntu22-4-sgx1\Virtual Machines\' -Filter *.vmcx -name) -Copy -GenerateNewId -VhdDestinationPath .\Ubuntu22-4-sgx1 

#Rename-VM -Name Ubuntu22-4-sgx1 -Newname Ubuntu22-4-sgx1-2

##enable sgx
Import-Module .\sgx.psm1

Set-VMSgx -VmName Ubuntu22-4-sgx1 -SgxEnabled $True -SgxSize 32 -SgxLaunchControlMode 0
#Set-VMSgx -VmName Ubuntu22-4-sgx1-2 -SgxEnabled $True -SgxSize 32 -SgxLaunchControlMode 0
Get-VMSgx -VmName Ubuntu22-4-sgx1
#Get-VMSgx -VmName Ubuntu22-4-sgx1-2

##start vm
Start-VM -Name Ubuntu22-4-sgx1
#Start-VM -Name Ubuntu22-4-sgx1-2

date >>log

##login to vm
Start-Sleep -s 10
ssh -i $HOME\.ssh\id_rsa ubuntu@$(.\convert.ps1) -o "StrictHostKeyChecking no"
#ssh -i $HOME\.ssh\id_rsa ubuntu@$(.\convert2.ps1) -o "StrictHostKeyChecking no"
