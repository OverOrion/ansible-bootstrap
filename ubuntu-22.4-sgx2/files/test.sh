#!/bin/bash


#phase1 30 sec
#helm uninstall redis -n marblerun
#marblerun uninstall
#exit
#exit
#sleep 30

##phase2
#marblerun install --domain $(hostname -A) --dcap-qpl intel --dcap-pccs-url https://$(hostname -A) --dcap-secure-cert FALSE
#kubectl get services -n marblerun

#echo nodeport
#exit
#######
port=30899

cd ./redis && openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=node-2.cluster.securitee.tech" -keyout admin_private.key  -out admin_certificate.crt
cd ..
cd ./redis && jq --arg cert "$(awk 'NF {sub(/\r/, ""); printf "%s\n",$0;}' admin_certificate.crt)" '.Users.redis.Certificate = $cert' marblerun.manifest.json.template > marblerun.manifest.json
cd ..
cd ./redis && marblerun manifest set marblerun.manifest.json node-2.cluster.securitee.tech:$port
cd ..
cd ./redis && marblerun manifest verify  marblerun.manifest.json node-2.cluster.securitee.tech:$port
cd ..
helm upgrade --install -f ./redis/kubernetes/values.yaml redis ./redis/kubernetes --create-namespace -n marblerun
